################################################################
# Method 1: MCODE (Sigma, Modifier v1)                         #
################################################################

Args <- commandArgs(TRUE)
library(MODifieR, lib.loc = "~/R/x86_64-pc-linux-gnu-library/Modifier_v1")

input1 <- read.csv(paste(Args[1], sep = ""), sep = ",", dec = ".", row.names = 1, stringsAsFactors = FALSE)

output_genes <- list()
sample_name <- colnames(input1[as.numeric(Args[2])])
input1_df <- data.frame(GS = rownames(input1), P.Val = (-log10(input1[, as.numeric(Args[2])])), 
                              stringsAsFactors = FALSE)
input1_df <- input1_df[order(input1_df$P.Val, decreasing = TRUE), ]
module_sum <- mod_mcode(deg_genes = input1_df, type = "Gene symbol", 
                        hierarchy      = 1,      # Hierarchical level of the PPI network to be used
                        vwp            = 0.5,    # Vertex Weight Percentage
                        haircut        = FALSE,  # Boolean, if TRUE removes singly connected nodes from clusters
                        fluff          = FALSE,  # Boolean, if TRUE expands cluster cores by one neighbour shell outwards
                        fdt            = 0.8,    # Cluster density cutoff
                        loops          = TRUE,   # Boolean, if TRUE includes self-loops
                        diffgen_cutoff = 1.3,    # Threshold (expression) for genes to be considered DEGs
                        module_cutoff  = 3.5)    # Threshold (score) for output modules
output_genes <- module_sum[[1]]$module_genes
write.table(output_genes, paste0("~/Documents/GSE83_v2/Modifier_v1/mcode_results/", sample_name, ".txt", sep = ""), 
            sep = "\t", row.names = FALSE, quote = FALSE)