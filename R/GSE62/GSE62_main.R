# Version info: R 3.2.3, Biobase 2.30.0, GEOquery 2.40.0, limma 3.26.8
# R scripts generated  Mon Sep 4 05:37:28 EDT 2017

rm(list = ls())
memory.limit(size = 50000)

################################################################
#Preliminary differential expression analysis with limma
#source("https://bioconductor.org/biocLite.R")
#biocLite("Biobase")
library(Biobase)
#biocLite("GEOquery")
library(GEOquery)
#biocLite("limma")
library(limma)

#Set the working directory
setwd("C:/Users/dmart/Documents/R/Master_Thesis/GSE62")

#Load series and platform data from GEO
gset <- getGEO("GSE76262", destdir="DATA", GSEMatrix=TRUE, AnnotGPL=TRUE)
cel_gset <- getGEOSuppFiles("GSE76262", makeDirectory=FALSE, baseDir="C:/Users/dmart/Documents/R/Master_Thesis/GSE62/RAWDATA")
if (length(gset) > 1) idx <- grep("GPL13158", attr(gset, "names")) else idx <- 1
gset <- gset[[idx]]

#Make proper column names to match toptable 
fvarLabels(gset) <- make.names(fvarLabels(gset))

#Group names for all samples
gsms <- paste0("22222222222222222222222222222222222222222222222222",
               "22222222222222222222222222222222222222222221111111",
               "111111111111111111000000000000000000000")
sml <- c()
for (i in 1:nchar(gsms)) { sml[i] <- substr(gsms,i,i) }

#log2 transform
ex <- exprs(gset)
qx <- as.numeric(quantile(ex, c(0., 0.25, 0.5, 0.75, 0.99, 1.0), na.rm=T))
LogC <- (qx[5] > 100) ||
  (qx[6]-qx[1] > 50 && qx[2] > 0) ||
  (qx[2] > 0 && qx[2] < 1 && qx[4] > 1 && qx[4] < 2)
if (LogC) { ex[which(ex <= 0)] <- NaN
exprs(gset) <- log2(ex) }

#Set up the data and proceed with analysis
sml <- paste("G", sml, sep="")    #Set group names
fl <- as.factor(sml)
gset$description <- fl
design <- model.matrix(~ description + 0, gset)
colnames(design) <- levels(fl)
fit <- lmFit(gset, design)
cont.matrix <- makeContrasts(G2-G0, G1-G0, G2-G1, levels=design)
fit2 <- contrasts.fit(fit, cont.matrix)
fit2 <- eBayes(fit2, 0.01)
tT <- topTable(fit2, adjust="fdr", sort.by="B", number=250)

tT <- subset(tT, select=c("ID","adj.P.Val","P.Value","F","Gene.symbol","Gene.title"))
write.table(tT, file=stdout(), row.names=F, sep="\t")

################################################################
#Boxplot for selected GEO samples

#Order samples by group
ex <- exprs(gset)[ , order(sml)]
sml <- sml[order(sml)]
fl <- as.factor(sml)
labels <- c("H_IS","MA_IS","SA_IS")

#Set parameters and draw the plot
palette(c("#dfeaf4","#f4dfdf","#f2cb98", "#AABBCC"))
dev.new(width=4+dim(gset)[[2]]/5, height=6)
par(mar=c(2+round(max(nchar(sampleNames(gset)))/2),4,2,1))
title <- paste ("GSE76262", '/', annotation(gset), " selected samples", sep ='')
boxplot(ex, boxwex=0.6, notch=T, main=title, outline=FALSE, las=2, col=fl)
legend("topleft", labels, fill=palette(), bty="n")

################################################################
#Quality analysis with AffyQC (default) / RMA (alternative 1) / GCRMA (alternative 2)
#biocLite("affy")
library(affy)

#Untar CEL files
untar("RAWDATA/GSE76262_RAW.tar", exdir="RAWDATA/CEL")
cels <- list.files(path="RAWDATA/CEL", pattern = "[gz]")
length(cels)

#Apply RMA to raw data (alt 1)
rawcel <- ReadAffy(celfile.path="C:/Users/dmart/Documents/R/Master_Thesis/GSE62/RAWDATA/CEL")
PM <- probes(rawcel, which="pm")
AffyInfo <- dimnames(PM)[[1]]
eset <- rma(rawcel, affinity.info=AffyInfo)
plot(density(exprs(eset)))

#Apply GCRMA to raw data (alt 2)
#biocLite("gcrma")
library(gcrma)
rawcel <- ReadAffy(celfile.path="C:/Users/dmart/Documents/R/Master_Thesis/GSE62/RAWDATA/CEL")
PM <- probes(rawcel, which="pm")
AffyInfo <- dimnames(PM)[[1]]
eset <- gcrma(rawcel, affinity.info=AffyInfo, optical.correct=TRUE)
plot(density(exprs(eset)))

#Apply AffyQC to raw data (default)
pheno <- gset@phenoData@data
phenosum <- within(pheno, rm("title", "status", "submission_date", "last_update_date", "channel_count", "organism_ch1", "molecule_ch1", "extract_protocol_ch1", "label_ch1", "label_protocol_ch1", "taxid_ch1", "hyb_protocol", "scan_protocol", "description", "data_processing", "platform_id", "contact_name", "contact_email", "contact_department", "contact_institute", "contact_address", "contact_city", "contact_zip/postal_code", "contact_country", "supplementary_file", "data_row_count"))
label <- as.factor(phenosum$geo_accession)
cohort <- as.factor(phenosum$source_name_ch1)
desc.df <- data.frame(Sample=cels,Label=label,Cohort=cohort)
write.table(desc.df,"description.txt",quote=FALSE,sep="\t",row.names=FALSE)
source("affyAnalysisQC.R")

################################################################
#Linear Models for Microarray Data (limma)
#biocLite("limma")
library(limma)

#Create design matrix
gsms <- paste0("22222222222222222222222222222222222222222222222222",
               "22222222222222222222222222222222222222222221111111",
               "111111111111111111000000000000000000000")
sml <- c()
for (i in 1:nchar(gsms)) { sml[i] <- substr(gsms,i,i) }
sml <- paste("G", sml, sep="")    #Set group names
fl <- as.factor(sml)
gset$description <- fl
design <- model.matrix(~ description + 0, gset)
colnames(design) <- levels(fl)
fit <- lmFit(eset, design)
cont.matrix <- makeContrasts(G2-G0, G1-G0, G2-G1, levels=design)
fit2 <- contrasts.fit(fit, cont.matrix)
fit2 <- eBayes(fit2, 0.01)
fulltt <- topTable(fit2, adjust="fdr", sort.by="F", number=Inf)

#Adjustment of p-values
#biocLite("qvalue")
library(qvalue)
sig_fulltt <- fulltt[fulltt$P.Value < 0.05, ]
sig_fulltt$adj.P.Val <- p.adjust(sig_fulltt$P.Value , method = "fdr")
fulltt$adj.P.Val <- p.adjust(fulltt$P.Value , method = "fdr")

#Generate volcano plots to define optimal logFC threshold for up-/down-regulated genes
write.csv(fulltt, file="volcano.txt")
volc_txt <- read.table("volcano.txt", sep=",", header=TRUE)

#volcano_plot1 (SA_IS vs H_IS)
volc1_plot <- with(volc_txt, plot(G2...G0, -log10(P.Value), pch=20, main="Volcano Plot", xlim=c(-3,3), xlab="logFC SA_IS vs H_IS", ylab="-log10(p-value)", cex=0.5))
#Add colored points: red if p-value<0.05, orange if abs(log2FC)>1, green if both
with(subset(volc_txt, P.Value < 0.05 ), points(G2...G0, -log10(P.Value), pch=20, col="red", cex=0.5))
with(subset(volc_txt, abs(G2...G0) > 1), points(G2...G0, -log10(P.Value), pch=20, col="orange", cex=1))
with(subset(volc_txt, P.Value < 0.05 & abs(G2...G0) > 1), points(G2...G0, -log10(P.Value), pch=20, col="green", cex=1))
abline(a=1.301, b=0, v=c(1,-1), lty=2)
#Write report based on limma results
library(xlsx)
write.xlsx(exprs(eset)[rownames(sig_fulltt)[sig_fulltt$adj.P.Val < 0.05 & abs(sig_fulltt$G2...G0) > 1], ], "Limma Analysis Results SA_IS vs H_IS.xlsx", sheetName="DEG Expression Matrix")
write.xlsx(eset@phenoData@data, "Limma Analysis Results SA_IS vs H_IS.xlsx", sheetName="Sample Annotation", append=TRUE)
write.xlsx(sig_fulltt[sig_fulltt$G2...G0 > 1,], "Limma Analysis Results SA_IS vs H_IS.xlsx", sheetName="Up-regulated genes", append=TRUE)
write.xlsx(sig_fulltt[sig_fulltt$adj.P.Val < 0.05 & sig_fulltt$G2...G0 < -1,], "Limma Analysis Results SA_IS vs H_IS.xlsx", sheetName="Down-regulated genes", append=TRUE)

#volcano_plot2 (MA_IS vs H_IS)
volc2_plot <- with(volc_txt, plot(G1...G0, -log10(P.Value), pch=20, main="Volcano Plot", xlim=c(-2,2), xlab="logFC MA_IS vs H_IS", ylab="-log10(p-value)", cex=0.5))
#Add colored points: red if p-value<0.05, orange if abs(log2FC)>1, green if both
with(subset(volc_txt, P.Value < 0.05 ), points(G1...G0, -log10(P.Value), pch=20, col="red", cex=0.5))
with(subset(volc_txt, abs(G1...G0) > 1), points(G1...G0, -log10(P.Value), pch=20, col="orange", cex=1))
with(subset(volc_txt, P.Value < 0.05 & abs(G1...G0) > 1), points(G1...G0, -log10(P.Value), pch=20, col="green", cex=1))
abline(a=1.301, b=0, v=c(1,-1), lty=2)
#Write report based on limma results
library(xlsx)
write.xlsx(exprs(eset)[rownames(sig_fulltt)[sig_fulltt$adj.P.Val < 0.05 & abs(sig_fulltt$G1...G0) > 1], ], "Limma Analysis Results MA_IS vs H_IS.xlsx", sheetName="DEG Expression Matrix")
write.xlsx(eset@phenoData@data, "Limma Analysis Results MA_IS vs H_IS.xlsx", sheetName="Sample Annotation", append=TRUE)
write.xlsx(sig_fulltt[sig_fulltt$G1...G0 > 1,], "Limma Analysis Results MA_IS vs H_IS.xlsx", sheetName="Up-regulated genes", append=TRUE)
write.xlsx(sig_fulltt[sig_fulltt$adj.P.Val < 0.05 & sig_fulltt$G1...G0 < -1,], "Limma Analysis Results MA_IS vs H_IS.xlsx", sheetName="Down-regulated genes", append=TRUE)

#volcano_plot3 (SA_IS vs MA_IS)
volc3_plot <- with(volc_txt, plot(G2...G1, -log10(P.Value), pch=20, main="Volcano Plot", xlim=c(-3,3), xlab="logFC SA_IS vs MA_IS", ylab="-log10(p-value)", cex=0.5))
#Add colored points: red if p-value<0.05, orange if abs(log2FC)>1, green if both
with(subset(volc_txt, P.Value < 0.05 ), points(G2...G1, -log10(P.Value), pch=20, col="red", cex=0.5))
with(subset(volc_txt, abs(G2...G1) > 1), points(G2...G1, -log10(P.Value), pch=20, col="orange", cex=1))
with(subset(volc_txt, P.Value < 0.05 & abs(G2...G1) > 1), points(G2...G1, -log10(P.Value), pch=20, col="green", cex=1))
abline(a=1.301, b=0, v=c(1,-1), lty=2)
#Write report based on limma results
library(xlsx)
write.xlsx(exprs(eset)[rownames(sig_fulltt)[sig_fulltt$adj.P.Val < 0.05 & abs(sig_fulltt$G2...G1) > 1], ], "Limma Analysis Results SA_IS vs MA_IS.xlsx", sheetName="DEG Expression Matrix")
write.xlsx(eset@phenoData@data, "Limma Analysis Results SA_IS vs MA_IS.xlsx", sheetName="Sample Annotation", append=TRUE)
write.xlsx(sig_fulltt[sig_fulltt$G2...G1 > 1,], "Limma Analysis Results SA_IS vs MA_IS.xlsx", sheetName="Up-regulated genes", append=TRUE)
write.xlsx(sig_fulltt[sig_fulltt$adj.P.Val < 0.05 & sig_fulltt$G2...G1 < -1,], "Limma Analysis Results SA_IS vs MA_IS.xlsx", sheetName="Down-regulated genes", append=TRUE)
