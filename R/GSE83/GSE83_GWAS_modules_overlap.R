# Identification of gene overlap between microarray expression profiling and GWAS

## GWAS (SNP-associated genes)
library(stringi)
library(dplyr)
library(tidyr)
library(org.Hs.eg.db)
library(annotate)

# PASCAL input 2 (assoc. gene + p-value) for healthy samples
setwd("/home/dme/Documents/R/Master_Thesis/GSE83/G83_PASCAL_Input2/H/")
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(snp_sig_H)) %do% {
  input_H_ <- matrix(data = 0, nrow = nrow(snp_sig_H), ncol = 2)
  input_H_[, 1] <- LR_sig_HvsA[, 16]
  input_H_[, 2] <- snp_sig_H[, i]
  for(j in 1:nrow(LR_sig_HvsA)) if (input_H_[j, 2] == "NA") {
    input_H_[j, 2] = 1
  }
  for(j in 1:nrow(LR_sig_HvsA)) if (input_H_[j, 2] == "AA") {
    input_H_[j, 2] = LR_sig_HvsA[j, 11]
  }
  for(j in 1:nrow(LR_sig_HvsA)) if (input_H_[j, 2] == "AB") {
    input_H_[j, 2] = LR_sig_HvsA[j, 12]
  }
  for(j in 1:nrow(LR_sig_HvsA)) if (input_H_[j, 2] == "BB") {
    input_H_[j, 2] = LR_sig_HvsA[j, 13]
  }
  colnames(input_H_) <- c("Assoc.Gene", "p_val")
  input_H_ <- as.data.frame(input_H_)
  input_H_ <- input_H_ %>% mutate(Assoc.Gene = Assoc.Gene %>% stri_split_fixed(",")) %>% unnest(Assoc.Gene)
  input_H_ <- subset(input_H_, p_val != 1)
  input_H_ <- input_H_[order(input_H_$Assoc.Gene, input_H_$p_val), ]
  input_H_ <- input_H_[!duplicated(input_H_$Assoc.Gene), ]
  #input_H_[, 3] <- annot_chart$Entrez_ID[match(input_H_[, 2], annot_chart$Gene_Symbol)]
  input_H_ <- as.matrix(input_H_)
  assign(paste("input_H_", colnames(snp_sig_H)[i], sep = ""), input_H_)
  input_name <- paste("input_H_", colnames(snp_sig_H)[i], ".txt", sep = "")
  #write.table(input_H_, file = input_name, row.names = FALSE, col.names = FALSE, quote = FALSE, 
  #sep = "\t", dec = ".")
  print(paste("input_H_", colnames(snp_sig_H)[i], " has been created", sep = ""))
}
stopCluster(cl)

# PASCAL input 2 (assoc. gene + p-value) for asthmatic samples
setwd("/home/dme/Documents/R/Master_Thesis/GSE83/G83_PASCAL_Input2/A/")
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(snp_sig_A)) %do% {
  input_A_ <- matrix(data = 0, nrow = nrow(snp_sig_A), ncol = 2)
  input_A_[, 1] <- LR_sig_HvsA[, 16]
  input_A_[, 2] <- snp_sig_A[, i]
  for(j in 1:nrow(LR_sig_HvsA)) if (input_A_[j, 2] == "NA") {
    input_A_[j, 2] = 1
  }
  for(j in 1:nrow(LR_sig_HvsA)) if (input_A_[j, 2] == "AA") {
    input_A_[j, 2] = LR_sig_HvsA[j, 11]
  }
  for(j in 1:nrow(LR_sig_HvsA)) if (input_A_[j, 2] == "AB") {
    input_A_[j, 2] = LR_sig_HvsA[j, 12]
  }
  for(j in 1:nrow(LR_sig_HvsA)) if (input_A_[j, 2] == "BB") {
    input_A_[j, 2] = LR_sig_HvsA[j, 13]
  }
  colnames(input_A_) <- c("Assoc.Gene", "p_val")
  input_A_ <- as.data.frame(input_A_)
  input_A_ <- input_A_ %>% mutate(Assoc.Gene = Assoc.Gene %>% stri_split_fixed(",")) %>% unnest(Assoc.Gene)
  input_A_ <- subset(input_A_, p_val != 1)
  input_A_ <- input_A_[order(input_A_$Assoc.Gene, input_A_$p_val), ]
  input_A_ <- input_A_[!duplicated(input_A_$Assoc.Gene), ]
  #input_A_[, 3] <- annot_chart$Entrez_ID[match(input_A_[, 2], annot_chart$Gene_Symbol)]
  input_A_ <- as.matrix(input_A_)
  assign(paste("input_A_", colnames(snp_sig_A)[i], sep = ""), input_A_)
  input_name <- paste("input_A_", colnames(snp_sig_A)[i], ".txt", sep = "")
  #write.table(input_A_, file = input_name, row.names = FALSE, col.names = FALSE, quote = FALSE, 
  #sep = "\t", dec = ".")
  print(paste("input_A_", colnames(snp_sig_A)[i], " has been created", sep = ""))
}
stopCluster(cl)

## MODifieR module genes
# Reading MODifieR output files as ID lists (MC = MCODE, CS = Clique Sum, D = DIAMOnD)
setwd("/home/dme/Documents/R/Master_Thesis/GSE83/mcode_results (1vsAll)")
filelist_MC <- list.files(path = "", pattern = "*.txt")
datalist_MC <- lapply(filelist_MC, FUN = read.table, header = TRUE)
names_MC <- substr(filelist_MC, 1, 10)
names(datalist_MC) <- names_MC
setwd("/home/dme/Documents/R/Master_Thesis/GSE83/cliquesum_results (1vsAll)")
filelist_CS <- list.files(pattern = "*.txt")
datalist_CS <- lapply(filelist_CS, FUN = read.table, header = TRUE)
names_CS <- substr(filelist_CS, 1, 10)
names(datalist_CS) <- names_CS
setwd("/home/dme/Documents/R/Master_Thesis/GSE83/diamond_results (1vsAll)")
filelist_D <- list.files(pattern = "*.txt")
datalist_D <- lapply(filelist_D, FUN = read.table, header = TRUE)
names_D <- substr(filelist_D, 1, 10)
names(datalist_D) <- names_D

# Sample classifier (H / MA / SA)
endotype_list <- read.csv(file = "/home/dme/Desktop/Master/Master Thesis/Data/endotype_list_updated.csv", header = TRUE, 
                          sep = ",", dec = ".", stringsAsFactors = FALSE)
patient_classifier <- endotype_list[complete.cases(endotype_list[, 1]), ][, c(1, 34)]
patient_classifier$blood[grep("Healthy|healthy", patient_classifier$blood)] <- "H"
patient_classifier$blood[grep("Moderate|moderate", patient_classifier$blood)] <- "MA"
patient_classifier$blood[grep("Severe|severe", patient_classifier$blood)] <- "SA"

setwd("/home/dme/Documents/PASCAL/output/G83_PASCAL_Module_genes_combinations/PathwaySet/A")
file_output_A <- list.files(pattern = "*.txt")
data_output_A <- lapply(file_output_A, FUN = read.table, header = TRUE)
names_output_A <- substr(file_output_A, 9, 18)
setwd("/home/dme/Documents/PASCAL/output/G83_PASCAL_Module_genes_combinations/PathwaySet/H")
file_output_H <- list.files(pattern = "*.txt")
data_output_H <- lapply(file_output_H, FUN = read.table, header = TRUE)
names_output_H <- substr(file_output_H, 9, 18)

names_output <- c(names_output_A, names_output_H)
data_output <- c(data_output_A, data_output_H)

# Output selection (combination and frequency calculation)
combination_selection <- "MC_U_CS_U_D"
                                        # optimal = combination with lower PASCAL p-value for each sample
                                        # MC = MCODE
                                        # CS = Clique Sum
                                        # D = DIAMOnD
                                        # MC_I_CS = Intersection MCODE & Clique Sum
                                        # MC_U_CS = Union MCODE & Clique Sum
                                        # MC_I_D = Intersection MCODE & DIAMOnD
                                        # MC_U_D = Union MCODE & DIAMOnD
                                        # CS_I_D = Intersection Clique Sum & DIAMOnD
                                        # CS_U_D = Union Clique Sum & DIAMOnD
                                        # MC_I_CS_I_D = Intersection MCODE & Clique Sum & DIAMOnD
                                        # MC_U_CS_U_D = Union MCODE & Clique Sum & DIAMOnD
combination_frequency <- "no"

# Improved combination selection and classification of samples
for (i in 1:length(names_output)) {
  print("--------------------------------------------------")
  print(names_output[i])
  sample_GSM <- names_output[i]
  sample_MC <- t(as.data.frame(subset(datalist_MC, names(datalist_MC) == sample_GSM)))
  sample_CS <- t(as.data.frame(subset(datalist_CS, names(datalist_CS) == sample_GSM)))
  sample_D <- t(as.data.frame(subset(datalist_D, names(datalist_D) == sample_GSM)))
  mod_comb <- as.data.frame(data_output[i])
  if (combination_selection == "optimal" && combination_frequency == "yes") {
    if ((patient_classifier$blood[which(patient_classifier$Accession %in% sample_GSM)] == "H") == TRUE) {
      print("Sample category: Healthy")
      #setwd("/home/dme/Documents/PASCAL/G83_Optimal_combinations/Improved/H/")
      if (mod_comb[1, 2] <= 0.05) {
        sig_comb <- as.character(mod_comb[1, 1])
        if (sig_comb == "MC") {
          opt_comb <- sample_MC
          freq_comb_H[1, 2] <- freq_comb_H[1, 2] + 1
          print("Optimal combination: MC")
        } else if (sig_comb == "CS") {
          opt_comb <- sample_CS
          freq_comb_H[2, 2] <- freq_comb_H[2, 2] + 1
          print("Optimal combination: CS")
        } else if (sig_comb == "D") {
          opt_comb <- sample_D
          freq_comb_H[3, 2] <- freq_comb_H[3, 2] + 1
          print("Optimal combination: D")
        } else if (sig_comb == "MC_I_CS") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_H[4, 2] <- freq_comb_H[4, 2] + 1
          print("Optimal combination: MC_I_CS")
        } else if (sig_comb == "MC_U_CS") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_H[5, 2] <- freq_comb_H[5, 2] + 1
          print("Optimal combination: MC_U_CS")
        } else if (sig_comb == "MC_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_H[6, 2] <- freq_comb_H[6, 2] + 1
          print("Optimal combination: MC_I_D")
        } else if (sig_comb == "MC_U_D") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_H[7, 2] <- freq_comb_H[7, 2] + 1
          print("Optimal combination: MC_U_D")
        } else if (sig_comb == "CS_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_H[8, 2] <- freq_comb_H[8, 2] + 1
          print("Optimal combination: CS_I_D")
        } else if (sig_comb == "CS_U_D") {
          opt_comb <- t(as.matrix(union(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_H[9, 2] <- freq_comb_H[9, 2] + 1
          print("Optimal combination: CS_U_D")
        } else if (sig_comb == "MC_I_CS_I_D") {
          opt_comb_b <- intersect(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(intersect(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_H[10, 2] <- freq_comb_H[10, 2] + 1
          print("Optimal combination: MC_I_CS_I_D")
        } else if (sig_comb == "MC_U_CS_U_D") {
          opt_comb_b <- union(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(union(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_H[11, 2] <- freq_comb_H[11, 2] + 1
          print("Optimal combination: MC_U_CS_U_D")
        } else {
          print(paste0("An error was found for ", sample_GSM))
        }
        opt_comb_ <- as.data.frame(t(opt_comb))
        for (i in 1:length(rownames(opt_comb_))) {
          opt_comb_[i, 2] <- getSYMBOL(as.character(opt_comb_[i, 1]), data = "org.Hs.eg")
        }
        colnames(opt_comb_)[2] <- "HGNC_Symbol"
        assign(paste("opt_comb_", sample_GSM, sep = ""), opt_comb_)
        opt_comb_name <- paste("opt_comb_", sample_GSM, ".txt", sep = "")
        #write.table(opt_comb_, file = opt_comb_name, row.names = TRUE, col.names = FALSE, quote = FALSE,
                    #sep = " ", dec = ".")
        print(paste(opt_comb_name, " has been created", sep = ""))
      }
      else {
        print(paste0("No significant combination was found for ", sample_GSM))
        freq_comb_H[12, 2] <- freq_comb_H[12, 2] + 1
      }
    } else if ((patient_classifier$blood[which(patient_classifier$Accession %in% sample_GSM)] == "MA") == TRUE) {
      print("Sample category: Moderate Asthma")
      #setwd("/home/dme/Documents/PASCAL/G83_Optimal_combinations/Improved/MA/")
      if (mod_comb[1, 2] <= 0.05) {
        sig_comb <- as.character(mod_comb[1, 1])
        if (sig_comb == "MC") {
          opt_comb <- sample_MC
          freq_comb_MA[1, 2] <- freq_comb_MA[1, 2] + 1
          print("Optimal combination: MC")
        } else if (sig_comb == "CS") {
          opt_comb <- sample_CS
          freq_comb_MA[2, 2] <- freq_comb_MA[2, 2] + 1
          print("Optimal combination: CS")
        } else if (sig_comb == "D") {
          opt_comb <- sample_D
          freq_comb_MA[3, 2] <- freq_comb_MA[3, 2] + 1
          print("Optimal combination: D")
        } else if (sig_comb == "MC_I_CS") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_MA[4, 2] <- freq_comb_MA[4, 2] + 1
          print("Optimal combination: MC_I_CS")
        } else if (sig_comb == "MC_U_CS") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_MA[5, 2] <- freq_comb_MA[5, 2] + 1
          print("Optimal combination: MC_U_CS")
        } else if (sig_comb == "MC_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_MA[6, 2] <- freq_comb_MA[6, 2] + 1
          print("Optimal combination: MC_I_D")
        } else if (sig_comb == "MC_U_D") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_MA[7, 2] <- freq_comb_MA[7, 2] + 1
          print("Optimal combination: MC_U_D")
        } else if (sig_comb == "CS_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_MA[8, 2] <- freq_comb_MA[8, 2] + 1
          print("Optimal combination: CS_I_D")
        } else if (sig_comb == "CS_U_D") {
          opt_comb <- t(as.matrix(union(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_MA[9, 2] <- freq_comb_MA[9, 2] + 1
          print("Optimal combination: CS_U_D")
        } else if (sig_comb == "MC_I_CS_I_D") {
          opt_comb_b <- intersect(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(intersect(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_MA[10, 2] <- freq_comb_MA[10, 2] + 1
          print("Optimal combination: MC_I_CS_I_D")
        } else if (sig_comb == "MC_U_CS_U_D") {
          opt_comb_b <- union(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(union(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_MA[11, 2] <- freq_comb_MA[11, 2] + 1
          print("Optimal combination: MC_U_CS_U_D")
        } else {
          print(paste0("An error was found for ", sample_GSM))
        }
        opt_comb_ <- as.data.frame(t(opt_comb))
        for (i in 1:length(rownames(opt_comb_))) {
          opt_comb_[i, 2] <- getSYMBOL(as.character(opt_comb_[i, 1]), data = "org.Hs.eg")
        }
        colnames(opt_comb_)[2] <- "HGNC_Symbol"
        assign(paste("opt_comb_", sample_GSM, sep = ""), opt_comb_)
        opt_comb_name <- paste("opt_comb_", sample_GSM, ".txt", sep = "")
        #write.table(opt_comb_, file = opt_comb_name, row.names = TRUE, col.names = FALSE, quote = FALSE,
                    #sep = " ", dec = ".")
        print(paste(opt_comb_name, " has been created", sep = ""))
      }
      else {
        print(paste0("No significant combination was found for ", sample_GSM))
        freq_comb_MA[12, 2] <- freq_comb_MA[12, 2] + 1
      }
    } else if ((patient_classifier$blood[which(patient_classifier$Accession %in% sample_GSM)] == "SA") == TRUE) {
      print("Sample category: Severe Asthma")
      #setwd("/home/dme/Documents/PASCAL/G83_Optimal_combinations/Improved/SA/")
      if (mod_comb[1, 2] <= 0.05) {
        sig_comb <- as.character(mod_comb[1, 1])
        if (sig_comb == "MC") {
          opt_comb <- sample_MC
          freq_comb_SA[1, 2] <- freq_comb_SA[1, 2] + 1
          print("Optimal combination: MC")
        } else if (sig_comb == "CS") {
          opt_comb <- sample_CS
          freq_comb_SA[2, 2] <- freq_comb_SA[2, 2] + 1
          print("Optimal combination: CS")
        } else if (sig_comb == "D") {
          opt_comb <- sample_D
          freq_comb_SA[3, 2] <- freq_comb_SA[3, 2] + 1
          print("Optimal combination: D")
        } else if (sig_comb == "MC_I_CS") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_SA[4, 2] <- freq_comb_SA[4, 2] + 1
          print("Optimal combination: MC_I_CS")
        } else if (sig_comb == "MC_U_CS") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_SA[5, 2] <- freq_comb_SA[5, 2] + 1
          print("Optimal combination: MC_U_CS")
        } else if (sig_comb == "MC_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_SA[6, 2] <- freq_comb_SA[6, 2] + 1
          print("Optimal combination: MC_I_D")
        } else if (sig_comb == "MC_U_D") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_SA[7, 2] <- freq_comb_SA[7, 2] + 1
          print("Optimal combination: MC_U_D")
        } else if (sig_comb == "CS_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_SA[8, 2] <- freq_comb_SA[8, 2] + 1
          print("Optimal combination: CS_I_D")
        } else if (sig_comb == "CS_U_D") {
          opt_comb <- t(as.matrix(union(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_SA[9, 2] <- freq_comb_SA[9, 2] + 1
          print("Optimal combination: CS_U_D")
        } else if (sig_comb == "MC_I_CS_I_D") {
          opt_comb_b <- intersect(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(intersect(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_SA[10, 2] <- freq_comb_SA[10, 2] + 1
          print("Optimal combination: MC_I_CS_I_D")
        } else if (sig_comb == "MC_U_CS_U_D") {
          opt_comb_b <- union(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(union(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          freq_comb_SA[11, 2] <- freq_comb_SA[11, 2] + 1
          print("Optimal combination: MC_U_CS_U_D")
        } else {
          print(paste0("An error was found for ", sample_GSM))
        }
        opt_comb_ <- as.data.frame(t(opt_comb))
        for (i in 1:length(rownames(opt_comb_))) {
          opt_comb_[i, 2] <- getSYMBOL(as.character(opt_comb_[i, 1]), data = "org.Hs.eg")
        }
        colnames(opt_comb_)[2] <- "HGNC_Symbol"
        assign(paste("opt_comb_", sample_GSM, sep = ""), opt_comb_)
        opt_comb_name <- paste("opt_comb_", sample_GSM, ".txt", sep = "")
        #write.table(opt_comb_, file = opt_comb_name, row.names = TRUE, col.names = FALSE, quote = FALSE,
                    #sep = " ", dec = ".")
        print(paste(opt_comb_name, " has been created", sep = ""))
      }
      else {
        print(paste0("No significant combination was found for ", sample_GSM))
        freq_comb_SA[12, 2] <- freq_comb_SA[12, 2] + 1
      }
    }
  } else if (combination_selection == "optimal" && combination_frequency == "no") {
    if ((patient_classifier$blood[which(patient_classifier$Accession %in% sample_GSM)] == "H") == TRUE) {
      print("Sample category: Healthy")
      #setwd("/home/dme/Documents/PASCAL/G83_Optimal_combinations/Improved/H/")
      if (mod_comb[1, 2] <= 0.05) {
        sig_comb <- as.character(mod_comb[1, 1])
        if (sig_comb == "MC") {
          opt_comb <- sample_MC
          print("Optimal combination: MC")
        } else if (sig_comb == "CS") {
          opt_comb <- sample_CS
          print("Optimal combination: CS")
        } else if (sig_comb == "D") {
          opt_comb <- sample_D
          print("Optimal combination: D")
        } else if (sig_comb == "MC_I_CS") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_I_CS")
        } else if (sig_comb == "MC_U_CS") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_U_CS")
        } else if (sig_comb == "MC_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_I_D")
        } else if (sig_comb == "MC_U_D") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_U_D")
        } else if (sig_comb == "CS_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: CS_I_D")
        } else if (sig_comb == "CS_U_D") {
          opt_comb <- t(as.matrix(union(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: CS_U_D")
        } else if (sig_comb == "MC_I_CS_I_D") {
          opt_comb_b <- intersect(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(intersect(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_I_CS_I_D")
        } else if (sig_comb == "MC_U_CS_U_D") {
          opt_comb_b <- union(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(union(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_U_CS_U_D")
        } else {
          print(paste0("An error was found for ", sample_GSM))
        }
        opt_comb_ <- as.data.frame(t(opt_comb))
        for (i in 1:length(rownames(opt_comb_))) {
          opt_comb_[i, 2] <- getSYMBOL(as.character(opt_comb_[i, 1]), data = "org.Hs.eg")
        }
        colnames(opt_comb_)[2] <- "HGNC_Symbol"
        assign(paste("opt_comb_", sample_GSM, sep = ""), opt_comb_)
        opt_comb_name <- paste("opt_comb_", sample_GSM, ".txt", sep = "")
        #write.table(opt_comb_, file = opt_comb_name, row.names = TRUE, col.names = FALSE, quote = FALSE,
                    #sep = " ", dec = ".")
        print(paste(opt_comb_name, " has been created", sep = ""))
      }
      else {
        print(paste0("No significant combination was found for ", sample_GSM))
      }
    } else if ((patient_classifier$blood[which(patient_classifier$Accession %in% sample_GSM)] == "MA") == TRUE) {
      print("Sample category: Moderate Asthma")
      #setwd("/home/dme/Documents/PASCAL/G83_Optimal_combinations/Improved/MA/")
      if (mod_comb[1, 2] <= 0.05) {
        sig_comb <- as.character(mod_comb[1, 1])
        if (sig_comb == "MC") {
          opt_comb <- sample_MC
          print("Optimal combination: MC")
        } else if (sig_comb == "CS") {
          opt_comb <- sample_CS
          print("Optimal combination: CS")
        } else if (sig_comb == "D") {
          opt_comb <- sample_D
          print("Optimal combination: D")
        } else if (sig_comb == "MC_I_CS") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_I_CS")
        } else if (sig_comb == "MC_U_CS") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_U_CS")
        } else if (sig_comb == "MC_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_I_D")
        } else if (sig_comb == "MC_U_D") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_U_D")
        } else if (sig_comb == "CS_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: CS_I_D")
        } else if (sig_comb == "CS_U_D") {
          opt_comb <- t(as.matrix(union(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: CS_U_D")
        } else if (sig_comb == "MC_I_CS_I_D") {
          opt_comb_b <- intersect(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(intersect(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_I_CS_I_D")
        } else if (sig_comb == "MC_U_CS_U_D") {
          opt_comb_b <- union(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(union(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_U_CS_U_D")
        } else {
          print(paste0("An error was found for ", sample_GSM))
        }
        opt_comb_ <- as.data.frame(t(opt_comb))
        for (i in 1:length(rownames(opt_comb_))) {
          opt_comb_[i, 2] <- getSYMBOL(as.character(opt_comb_[i, 1]), data = "org.Hs.eg")
        }
        colnames(opt_comb_)[2] <- "HGNC_Symbol"
        assign(paste("opt_comb_", sample_GSM, sep = ""), opt_comb_)
        opt_comb_name <- paste("opt_comb_", sample_GSM, ".txt", sep = "")
        #write.table(opt_comb_, file = opt_comb_name, row.names = TRUE, col.names = FALSE, quote = FALSE,
                    #sep = " ", dec = ".")
        print(paste(opt_comb_name, " has been created", sep = ""))
      }
      else {
        print(paste0("No significant combination was found for ", sample_GSM))
      }
    } else if ((patient_classifier$blood[which(patient_classifier$Accession %in% sample_GSM)] == "SA") == TRUE) {
      print("Sample category: Severe Asthma")
      #setwd("/home/dme/Documents/PASCAL/G83_Optimal_combinations/Improved/SA/")
      if (mod_comb[1, 2] <= 0.05) {
        sig_comb <- as.character(mod_comb[1, 1])
        if (sig_comb == "MC") {
          opt_comb <- sample_MC
          print("Optimal combination: MC")
        } else if (sig_comb == "CS") {
          opt_comb <- sample_CS
          print("Optimal combination: CS")
        } else if (sig_comb == "D") {
          opt_comb <- sample_D
          print("Optimal combination: D")
        } else if (sig_comb == "MC_I_CS") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_I_CS")
        } else if (sig_comb == "MC_U_CS") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_CS)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_U_CS")
        } else if (sig_comb == "MC_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_I_D")
        } else if (sig_comb == "MC_U_D") {
          opt_comb <- t(as.matrix(union(sample_MC, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_U_D")
        } else if (sig_comb == "CS_I_D") {
          opt_comb <- t(as.matrix(intersect(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: CS_I_D")
        } else if (sig_comb == "CS_U_D") {
          opt_comb <- t(as.matrix(union(sample_CS, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: CS_U_D")
        } else if (sig_comb == "MC_I_CS_I_D") {
          opt_comb_b <- intersect(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(intersect(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_I_CS_I_D")
        } else if (sig_comb == "MC_U_CS_U_D") {
          opt_comb_b <- union(sample_MC, sample_CS)
          opt_comb <- t(as.matrix(union(opt_comb_b, sample_D)))
          rownames(opt_comb) <- sample_GSM
          print("Optimal combination: MC_U_CS_U_D")
        } else {
          print(paste0("An error was found for ", sample_GSM))
        }
        opt_comb_ <- as.data.frame(t(opt_comb))
        for (i in 1:length(rownames(opt_comb_))) {
          opt_comb_[i, 2] <- getSYMBOL(as.character(opt_comb_[i, 1]), data = "org.Hs.eg")
        }
        colnames(opt_comb_)[2] <- "HGNC_Symbol"
        assign(paste("opt_comb_", sample_GSM, sep = ""), opt_comb_)
        opt_comb_name <- paste("opt_comb_", sample_GSM, ".txt", sep = "")
        #write.table(opt_comb_, file = opt_comb_name, row.names = TRUE, col.names = FALSE, quote = FALSE,
                    #sep = " ", dec = ".")
        print(paste(opt_comb_name, " has been created", sep = ""))
      }
      else {
        print(paste0("No significant combination was found for ", sample_GSM))
      }
    } else {
      print("Error: Sample could not be classified")
    }
  } else if (combination_selection != "optimal" && (patient_classifier$blood[which(patient_classifier$Accession %in% sample_GSM)] == "H") == TRUE) {
    print("Sample category: Healthy")
    #setwd("/home/dme/Documents/PASCAL/G83_Optimal_combinations/Improved/H/")
    if (combination_selection == "MC") {
      opt_comb <- sample_MC
      print("Selected combination: MC")
    } else if (combination_selection == "CS") {
      opt_comb <- sample_CS
      print("Selected combination: CS")
    } else if (combination_selection == "D") {
      opt_comb <- sample_D
      print("Selected combination: D")
    } else if (combination_selection == "MC_I_CS") {
      opt_comb <- t(as.matrix(intersect(sample_MC, sample_CS)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_I_CS")
    } else if (combination_selection == "MC_U_CS") {
      opt_comb <- t(as.matrix(union(sample_MC, sample_CS)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_U_CS")
    } else if (combination_selection == "MC_I_D") {
      opt_comb <- t(as.matrix(intersect(sample_MC, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_I_D")
    } else if (combination_selection == "MC_U_D") {
      opt_comb <- t(as.matrix(union(sample_MC, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_U_D")
    } else if (combination_selection == "CS_I_D") {
      opt_comb <- t(as.matrix(intersect(sample_CS, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: CS_I_D")
    } else if (combination_selection == "CS_U_D") {
      opt_comb <- t(as.matrix(union(sample_CS, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: CS_U_D")
    } else if (combination_selection == "MC_I_CS_I_D") {
      opt_comb_b <- intersect(sample_MC, sample_CS)
      opt_comb <- t(as.matrix(intersect(opt_comb_b, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_I_CS_I_D")
    } else if (combination_selection == "MC_U_CS_U_D") {
      opt_comb_b <- union(sample_MC, sample_CS)
      opt_comb <- t(as.matrix(union(opt_comb_b, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_U_CS_U_D")
    } else {
      print(paste0("An error was found for ", sample_GSM))
    }
    opt_comb_ <- as.data.frame(t(opt_comb))
    for (i in 1:length(rownames(opt_comb_))) {
      opt_comb_[i, 2] <- getSYMBOL(as.character(opt_comb_[i, 1]), data = "org.Hs.eg")
    }
    colnames(opt_comb_)[2] <- "HGNC_Symbol"
    assign(paste("opt_comb_", sample_GSM, sep = ""), opt_comb_)
    opt_comb_name <- paste("opt_comb_", sample_GSM, ".txt", sep = "")
    #write.table(opt_comb_, file = opt_comb_name, row.names = TRUE, col.names = FALSE, quote = FALSE,
                #sep = " ", dec = ".")
    print(paste(opt_comb_name, " has been created", sep = ""))
  } else if (combination_selection != "optimal" && (patient_classifier$blood[which(patient_classifier$Accession %in% sample_GSM)] == "MA") == TRUE) {
    print("Sample category: Moderate Asthma")
    #setwd("/home/dme/Documents/PASCAL/G83_Optimal_combinations/Improved/MA/")
    if (combination_selection == "MC") {
      opt_comb <- sample_MC
      print("Selected combination: MC")
    } else if (combination_selection == "CS") {
      opt_comb <- sample_CS
      print("Selected combination: CS")
    } else if (combination_selection == "D") {
      opt_comb <- sample_D
      print("Selected combination: D")
    } else if (combination_selection == "MC_I_CS") {
      opt_comb <- t(as.matrix(intersect(sample_MC, sample_CS)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_I_CS")
    } else if (combination_selection == "MC_U_CS") {
      opt_comb <- t(as.matrix(union(sample_MC, sample_CS)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_U_CS")
    } else if (combination_selection == "MC_I_D") {
      opt_comb <- t(as.matrix(intersect(sample_MC, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_I_D")
    } else if (combination_selection == "MC_U_D") {
      opt_comb <- t(as.matrix(union(sample_MC, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_U_D")
    } else if (combination_selection == "CS_I_D") {
      opt_comb <- t(as.matrix(intersect(sample_CS, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: CS_I_D")
    } else if (combination_selection == "CS_U_D") {
      opt_comb <- t(as.matrix(union(sample_CS, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: CS_U_D")
    } else if (combination_selection == "MC_I_CS_I_D") {
      opt_comb_b <- intersect(sample_MC, sample_CS)
      opt_comb <- t(as.matrix(intersect(opt_comb_b, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_I_CS_I_D")
    } else if (combination_selection == "MC_U_CS_U_D") {
      opt_comb_b <- union(sample_MC, sample_CS)
      opt_comb <- t(as.matrix(union(opt_comb_b, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_U_CS_U_D")
    } else {
      print(paste0("An error was found for ", sample_GSM))
    }
    opt_comb_ <- as.data.frame(t(opt_comb))
    for (i in 1:length(rownames(opt_comb_))) {
      opt_comb_[i, 2] <- getSYMBOL(as.character(opt_comb_[i, 1]), data = "org.Hs.eg")
    }
    colnames(opt_comb_)[2] <- "HGNC_Symbol"
    assign(paste("opt_comb_", sample_GSM, sep = ""), opt_comb_)
    opt_comb_name <- paste("opt_comb_", sample_GSM, ".txt", sep = "")
    #write.table(opt_comb_, file = opt_comb_name, row.names = TRUE, col.names = FALSE, quote = FALSE,
                #sep = " ", dec = ".")
    print(paste(opt_comb_name, " has been created", sep = ""))
  } else if (combination_selection != "optimal" && (patient_classifier$blood[which(patient_classifier$Accession %in% sample_GSM)] == "SA") == TRUE) {
    print("Sample category: Severe Asthma")
    #setwd("/home/dme/Documents/PASCAL/G83_Optimal_combinations/Improved/SA/")
    if (combination_selection == "MC") {
      opt_comb <- sample_MC
      print("Selected combination: MC")
    } else if (combination_selection == "CS") {
      opt_comb <- sample_CS
      print("Selected combination: CS")
    } else if (combination_selection == "D") {
      opt_comb <- sample_D
      print("Selected combination: D")
    } else if (combination_selection == "MC_I_CS") {
      opt_comb <- t(as.matrix(intersect(sample_MC, sample_CS)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_I_CS")
    } else if (combination_selection == "MC_U_CS") {
      opt_comb <- t(as.matrix(union(sample_MC, sample_CS)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_U_CS")
    } else if (combination_selection == "MC_I_D") {
      opt_comb <- t(as.matrix(intersect(sample_MC, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_I_D")
    } else if (combination_selection == "MC_U_D") {
      opt_comb <- t(as.matrix(union(sample_MC, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_U_D")
    } else if (combination_selection == "CS_I_D") {
      opt_comb <- t(as.matrix(intersect(sample_CS, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: CS_I_D")
    } else if (combination_selection == "CS_U_D") {
      opt_comb <- t(as.matrix(union(sample_CS, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: CS_U_D")
    } else if (combination_selection == "MC_I_CS_I_D") {
      opt_comb_b <- intersect(sample_MC, sample_CS)
      opt_comb <- t(as.matrix(intersect(opt_comb_b, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_I_CS_I_D")
    } else if (combination_selection == "MC_U_CS_U_D") {
      opt_comb_b <- union(sample_MC, sample_CS)
      opt_comb <- t(as.matrix(union(opt_comb_b, sample_D)))
      rownames(opt_comb) <- sample_GSM
      print("Selected combination: MC_U_CS_U_D")
    } else {
      print(paste0("An error was found for ", sample_GSM))
    }
    opt_comb_ <- as.data.frame(t(opt_comb))
    for (i in 1:length(rownames(opt_comb_))) {
      opt_comb_[i, 2] <- getSYMBOL(as.character(opt_comb_[i, 1]), data = "org.Hs.eg")
    }
    colnames(opt_comb_)[2] <- "HGNC_Symbol"
    assign(paste("opt_comb_", sample_GSM, sep = ""), opt_comb_)
    opt_comb_name <- paste("opt_comb_", sample_GSM, ".txt", sep = "")
    #write.table(opt_comb_, file = opt_comb_name, row.names = TRUE, col.names = FALSE, quote = FALSE,
                #sep = " ", dec = ".")
    print(paste(opt_comb_name, " has been created", sep = ""))
  } else {
    print("Error: check input files and/or input parameters")
  }
}

# Calculation of the overlap of matching samples
setwd("/home/dme/Documents/PASCAL/G83_GWAS_Modules_Overlap/Overlaps (MC_U_CS_U_D)")
opt_comb_length <- length(apropos("opt_comb_GSM", where = FALSE, ignore.case = TRUE, mode = "any"))
for (i in 1:opt_comb_length) {
  opt_comb_sample <- apropos(paste0("opt_comb_", names_output[i]), where = FALSE, ignore.case = TRUE, mode = "any")
  input_A_sample <- apropos(paste0("input_A_", names_output[i]), where = FALSE, ignore.case = TRUE, mode = "any")
  input_H_sample <- apropos(paste0("input_H_", names_output[i]), where = FALSE, ignore.case = TRUE, mode = "any")
  overlap_1 <- get(opt_comb_sample)
  if (length(input_H_sample) == 0L) {
    overlap_2 <- get(input_A_sample)
  } else {
    overlap_2 <- get(input_H_sample)
  }
  overlap_ <- as.data.frame(intersect(overlap_1[, 2], overlap_2[, 2]))
  colnames(overlap_)[1] <- "Overlap"
  assign(paste("overlap_", names_output[i], sep = ""), overlap_)
  write.table(overlap_, file = paste0("overlap_", names_output[i]), row.names = FALSE, col.names = FALSE, quote = FALSE,
              sep = " ", dec = ".")
  print(paste(names_output[i], " overlap file has been created", sep = ""))
}












#### Extract annotation info from GPL (array platform)
source("https://bioconductor.org/biocLite.R")
biocLite("GEOquery")
library(GEOquery)
gpl <- getGEO("GPL13158", destdir = "/home/dme/Documents/R/Master_Thesis/GSE83/")
annot_info <- Table(gpl)[, c(1, 11, 12)] # 16 levels/annotation formats
for (i in 1:length(colnames(annot_info))) {
  annot_info[, i] <- as.character(annot_info[, i])
}
colnames(annot_info) <- c("ID", "Gene_Symbol", "Entrez_ID")

#### Addition of missing ID information (NCBI)
annot_info[37172, 3] <- "619423 /// 105379219"
annot_info[8756, 2] <- "LOC100132057 /// LOC100286793 /// LINC01138 /// FAM91A2 /// LOC728855"
annot_info[8756, 3] <- "100132057 /// 100286793 /// 388685 /// 57234 /// 728855"
annot_info[41841, 3] <- "81176"
annot_info[6927, 3] <- "100379174"
annot_info[47966, 3] <- "619423"
annot_info[41724, 3] <- "619518"

#### Annotation Chart (HGNC symbol / Entrez ID)
annot_chart <- data.frame(Gene_Symbol = unlist(strsplit(as.character(annot_info$Gene_Symbol)," /// ")), 
                          Entrez_ID = unlist(strsplit(as.character(annot_info$Entrez_ID)," /// ")))
annot_chart <- annot_chart[unique(annot_chart$Gene_Symbol), ]
for (i in 1:length(colnames(annot_chart))) {
  annot_chart[, i] <- as.character(annot_chart[, i])
}


