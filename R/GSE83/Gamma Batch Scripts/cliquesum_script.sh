#!/bin/bash

#SBATCH -J clique_sum
#SBATCH -n 1
#SBATCH -t 00:40:00
#SBATCH --mem=4000

export OMP_NUM_THREADS=16
module load gcc/5.3.0
module load python/2.7.13-anaconda-4.4.0
module load R/3.4.1

R --no-restore --no-save CMD BATCH "--args $1 $2 $3" cliquesum_gamma.R
