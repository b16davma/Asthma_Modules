################################################################
# Method 4: DIAMOnD (gamma)                                    #
################################################################

Args <- commandArgs(TRUE)
#print(Args[1])
#print(Args[2])
#print(Args[3])
#print(Args[4])

## DIAMOnD (seed-gene-based algorithm to identify disease modules from DEGs)
# Input: Entrez IDs for genes (path) and network (path)
library(MODifieR, lib.loc = "/proj/lassim/disease_modules/rlibrary")
library(foreach)
library(parallel)
library(doParallel)
#setwd("/home/x_damar/Documents/R/Master_Thesis/GSE83")
matrix_K <- read.csv(paste(Args[1], sep = ""), sep = ",", dec = ".", row.names = 1, stringsAsFactors = FALSE)

# Identification of modules with diamond function
diamond_df <- matrix_K
diamond_module_genes <- list()
#cl = makeCluster(detectCores())
#registerDoParallel(cl)
#foreach(i = 1:ncol(diamond_df), .packages = "MODifieR") %do% {
diamond_name <- colnames(diamond_df[as.numeric(Args[4])])
#print(diamond_name)
diamond_module_df <- data.frame(GS = diamond_df$Entrez_ID, P.Val = (-log10(diamond_df[, as.numeric(Args[4])])), stringsAsFactors = FALSE)
rm(matrix_K)
rm(diamond_df)
diamond_module_df <- diamond_module_df[order(diamond_module_df$P.Val, decreasing = TRUE), ]
write.table(diamond_module_df[1:200, 1], file = "diamond_module_df.txt", col.names = TRUE, sep = "\t", dec = ".", row.names = FALSE)
diamond_module_sum <- diamond(network = paste0(Args[2]), input_genes = paste0(Args[3]), 
                              n_output_genes = 200,   # Maximum number of genes to include in the final module
                              seed_weight    = 1,     # Weight assignation for seed genes
                              include_seed   = TRUE)  # Boolean, if TRUE includes seed genes in the final module
diamond_module_genes <- diamond_module_sum$module_genes
write.table(diamond_module_genes, paste0("~/Documents/R/Master_Thesis/GSE83/diamond_results/", diamond_name, ".txt", sep = ""), sep = "\t", row.names = FALSE, quote = FALSE)
print(paste(diamond_name, "module has been created", sep = " "))
#}
