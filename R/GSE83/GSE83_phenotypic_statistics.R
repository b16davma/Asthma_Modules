################################################################
# U-BIOPRED baseline phenotypic statistical analysis           #
################################################################

setwd("/home/dme/Desktop/Master/Master Thesis/Data")
metadata_G83 <- read.csv("endotype_list_updated.csv", header = TRUE, stringsAsFactors = FALSE)
numeric_columns <- c(5:18, 25:33) # Select data columns to be converted to numeric class
for (i in numeric_columns){
  metadata_G83[, i] <- (metadata_G83[, i] = as.numeric(metadata_G83[, i]))
}
metadata_G83 <- metadata_G83[complete.cases(metadata_G83$blood), ]
sum_G83 <- matrix(data = NA, nrow = 71, ncol = 4)
colnames(sum_G83) <- c("H", "MA", "SA", "p-value")
rownames(sum_G83) <- c("Sample_size", "Female", "Female_%", "Age", "Age_SD", "BMI", "BMI_SD", "Smokers", "Smokers_%", 
                       "n_Total_IgE_(IU/ml)", "Total_IgE_(IU/ml)", "Total_IgE_(IU/ml)_Q1", "Total_IgE_(IU/ml)_Q3", 
                       "n_CRP_(mg/l)", "CRP_(mg/l)", "CRP_(mg/l)_Q1", "CRP_(mg/l)_Q3", 
                       "n_Blood_eosinophils_(10^3/ul)", "Blood_eosinophils_(10^3/ul)", "Blood_eosinophils_(10^3/ul)_Q1", "Blood_eosinophils_(10^3/ul)_Q3", 
                       "n_Blood_neutrophils_(10^3/ul)", "Blood_neutrophils_(10^3/ul)", "Blood_neutrophils_(10^3/ul)_Q1", "Blood_neutrophils_(10^3/ul)_Q3", 
                       "n_Periostin_(ng/ml)", "Periostin_(ng/ml)", "Periostin_(ng/ml)_Q1", "Periostin_(ng/ml)_Q3", 
                       "n_Sputum_eosinophils_%", "Sputum_eosinophils_%", "Sputum_eosinophils_%_Q1", "Sputum_eosinophils_%_Q3", 
                       "n_Sputum_neutrophils_%", "Sputum_neutrophils_%", "Sputum_neutrophils_%_Q1", "Sputum_neutrophils_%_Q3", 
                       "n_Sputum_macrophages_%", "Sputum_macrophages_%", "Sputum_macrophages_%_Q1", "Sputum_macrophages_%_Q3", 
                       "n_Sputum_lymphocytes_%", "Sputum_lymphocytes_%", "Sputum_lymphocytes_%_Q1", "Sputum_lymphocytes_%_Q3", 
                       "n_Sputum_mast_cells_%", "Sputum_mast_cells_%", "Sputum_mast_cells_%_Q1", "Sputum_mast_cells_%_Q3", 
                       "n_FeNO_(ppb)", "FeNO_(ppb)", "FeNO_(ppb)_Q1", "FeNO_(ppb)_Q3", 
                       "n_FEV1_%", "FEV1_%", "FEV1_%_Q1", "FEV1_%_Q3", 
                       "Eczema", "Eczema_%", "Allergic_rhinitis", "Allergic_rhinitis_%", 
                       "Atopy", "Atopy_%", "Sinusitis", "Sinusitis_%", 
                       "Nasal_polyps", "Nasal_polyps_%", "Oral_cortiscosteroid_use", "Oral_corticosteroid_use_%", 
                       "White_caucasian", "White_caucasian_%")

# Sample size (SA: severe asthma, MA: moderate asthma, H: healthy)
G83_H <- subset(metadata_G83, blood == "Healthy, non-smoking")
sum_G83[1, 1] <- nrow(G83_H)

G83_MA <- subset(metadata_G83, blood == "Moderate asthma, non-smoking")
sum_G83[1, 2] <- nrow(G83_MA)

G83_SA <- subset(metadata_G83, blood == "Severe asthma, non-smoking" | blood == "Severe asthma, smoking")
sum_G83[1, 3] <- nrow(G83_SA)

# Female
sum_G83[2, 1] <- nrow(subset(metadata_G83, Sex == "female" & blood == "Healthy, non-smoking"))
sum_G83[3, 1] <- round((sum_G83[2, 1]/sum_G83[1, 1])*100, digits = 1)

sum_G83[2, 2] <- nrow(subset(metadata_G83, Sex == "female" & blood == "Moderate asthma, non-smoking"))
sum_G83[3, 2] <- round((sum_G83[2, 2]/sum_G83[1, 2])*100, digits = 1)

sum_G83[2, 3] <- nrow(subset(metadata_G83, Sex == "female" & (blood == "Severe asthma, non-smoking" | blood == "Severe asthma, smoking")))
sum_G83[3, 3] <- round((sum_G83[2, 3]/sum_G83[1, 3])*100, digits = 1)

# Age (y)
sum_G83[4, 1] <- round(mean(G83_H$Age), digits = 0)
sum_G83[5, 1] <- round(sd(G83_H$Age), digits = 0)

sum_G83[4, 2] <- round(mean(G83_MA$Age), digits = 0)
sum_G83[5, 2] <- round(sd(G83_MA$Age), digits = 0)

sum_G83[4, 3] <- round(mean(G83_SA$Age), digits = 0)
sum_G83[5, 3] <- round(sd(G83_SA$Age), digits = 0)

# BMI (kg/m2)
sum_G83[6, 1] <- round(mean(G83_H$Body_Mass_Index_.kg.m2.), digits = 1)
sum_G83[7, 1] <- round(sd(G83_H$Body_Mass_Index_.kg.m2.), digits = 1)

sum_G83[6, 2] <- round(mean(G83_MA$Body_Mass_Index_.kg.m2.), digits = 1)
sum_G83[7, 2] <- round(sd(G83_MA$Body_Mass_Index_.kg.m2.), digits = 1)

sum_G83[6, 3] <- round(mean(G83_SA$Body_Mass_Index_.kg.m2.), digits = 1)
sum_G83[7, 3] <- round(sd(G83_SA$Body_Mass_Index_.kg.m2.), digits = 1)

# Smokers
sum_G83[8, 1] <- 0
sum_G83[9, 1] <- 0

sum_G83[8, 2] <- 0
sum_G83[9, 2] <- 0

sum_G83[8, 3] <- nrow(subset(metadata_G83, blood == "Severe asthma, smoking"))
sum_G83[9, 3] <- round(((sum_G83[8, 3]/sum_G83[1, 3])*100), digits = 1)

# Total IgE (IU/ml)
sum_G83[10, 1] <- round(nrow(G83_H[complete.cases(G83_H$IgE_Total_.IU.ml.), ] == TRUE), digits = 0)
sum_G83[11, 1] <- round(median(G83_H$IgE_Total_.IU.ml., na.rm = TRUE), digits = 1)
sum_G83[12, 1] <- round(quantile(G83_H$IgE_Total_.IU.ml., na.rm = TRUE, 0.25), digits = 1)
sum_G83[13, 1] <- round(quantile(G83_H$IgE_Total_.IU.ml., na.rm = TRUE, 0.75), digits = 1)

sum_G83[10, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$IgE_Total_.IU.ml.), ] == TRUE), digits = 0)
sum_G83[11, 2] <- round(median(G83_MA$IgE_Total_.IU.ml., na.rm = TRUE), digits = 1)
sum_G83[12, 2] <- round(quantile(G83_MA$IgE_Total_.IU.ml., na.rm = TRUE, 0.25), digits = 1)
sum_G83[13, 2] <- round(quantile(G83_MA$IgE_Total_.IU.ml., na.rm = TRUE, 0.75), digits = 1)

sum_G83[10, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$IgE_Total_.IU.ml.), ] == TRUE), digits = 0)
sum_G83[11, 3] <- round(median(G83_SA$IgE_Total_.IU.ml., na.rm = TRUE), digits = 1)
sum_G83[12, 3] <- round(quantile(G83_SA$IgE_Total_.IU.ml., na.rm = TRUE, 0.25), digits = 1)
sum_G83[13, 3] <- round(quantile(G83_SA$IgE_Total_.IU.ml., na.rm = TRUE, 0.75), digits = 1)

# C-reactive protein (mg/l)
sum_G83[14, 1] <- round(nrow(G83_H[complete.cases(G83_H$C_Reactive_Protein_.mg.L.), ] == TRUE), digits = 0)
sum_G83[15, 1] <- round(median(G83_H$C_Reactive_Protein_.mg.L., na.rm = TRUE), digits = 1)
sum_G83[16, 1] <- round(quantile(G83_H$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.25), digits = 1)
sum_G83[17, 1] <- round(quantile(G83_H$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.75), digits = 1)

sum_G83[14, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$C_Reactive_Protein_.mg.L.), ] == TRUE), digits = 0)
sum_G83[15, 2] <- round(median(G83_MA$C_Reactive_Protein_.mg.L., na.rm = TRUE), digits = 1)
sum_G83[16, 2] <- round(quantile(G83_MA$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.25), digits = 1)
sum_G83[17, 2] <- round(quantile(G83_MA$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.75), digits = 1)

sum_G83[14, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$C_Reactive_Protein_.mg.L.), ] == TRUE), digits = 0)
sum_G83[15, 3] <- round(median(G83_SA$C_Reactive_Protein_.mg.L., na.rm = TRUE), digits = 1)
sum_G83[16, 3] <- round(quantile(G83_SA$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.25), digits = 1)
sum_G83[17, 3] <- round(quantile(G83_SA$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.75), digits = 1)

# Blood eosinophils (10^3/ul)
sum_G83[18, 1] <- round(nrow(G83_H[complete.cases(G83_H$eosinophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G83[19, 1] <- round(median(G83_H$eosinophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G83[20, 1] <- round(quantile(G83_H$eosinophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G83[21, 1] <- round(quantile(G83_H$eosinophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

sum_G83[18, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$eosinophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G83[19, 2] <- round(median(G83_MA$eosinophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G83[20, 2] <- round(quantile(G83_MA$eosinophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G83[21, 2] <- round(quantile(G83_MA$eosinophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

sum_G83[18, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$eosinophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G83[19, 3] <- round(median(G83_SA$eosinophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G83[20, 3] <- round(quantile(G83_SA$eosinophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G83[21, 3] <- round(quantile(G83_SA$eosinophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

# Blood neutrophils (10^3/ul)
sum_G83[22, 1] <- round(nrow(G83_H[complete.cases(G83_H$neutrophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G83[23, 1] <- round(median(G83_H$neutrophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G83[24, 1] <- round(quantile(G83_H$neutrophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G83[25, 1] <- round(quantile(G83_H$neutrophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

sum_G83[22, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$neutrophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G83[23, 2] <- round(median(G83_MA$neutrophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G83[24, 2] <- round(quantile(G83_MA$neutrophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G83[25, 2] <- round(quantile(G83_MA$neutrophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

sum_G83[22, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$neutrophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G83[23, 3] <- round(median(G83_SA$neutrophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G83[24, 3] <- round(quantile(G83_SA$neutrophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G83[25, 3] <- round(quantile(G83_SA$neutrophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

# Periostin (ng/ml)
sum_G83[26, 1] <- round(nrow(G83_H[complete.cases(G83_H$Periostin_.ng.mL.), ] == TRUE), digits = 0)
sum_G83[27, 1] <- round(median(G83_H$Periostin_.ng.mL., na.rm = TRUE), digits = 1)
sum_G83[28, 1] <- round(quantile(G83_H$Periostin_.ng.mL., na.rm = TRUE, 0.25), digits = 1)
sum_G83[29, 1] <- round(quantile(G83_H$Periostin_.ng.mL., na.rm = TRUE, 0.75), digits = 1)

sum_G83[26, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$Periostin_.ng.mL.), ] == TRUE), digits = 0)
sum_G83[27, 2] <- round(median(G83_MA$Periostin_.ng.mL., na.rm = TRUE), digits = 1)
sum_G83[28, 2] <- round(quantile(G83_MA$Periostin_.ng.mL., na.rm = TRUE, 0.25), digits = 1)
sum_G83[29, 2] <- round(quantile(G83_MA$Periostin_.ng.mL., na.rm = TRUE, 0.75), digits = 1)

sum_G83[26, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$Periostin_.ng.mL.), ] == TRUE), digits = 0)
sum_G83[27, 3] <- round(median(G83_SA$Periostin_.ng.mL., na.rm = TRUE), digits = 1)
sum_G83[28, 3] <- round(quantile(G83_SA$Periostin_.ng.mL., na.rm = TRUE, 0.25), digits = 1)
sum_G83[29, 3] <- round(quantile(G83_SA$Periostin_.ng.mL., na.rm = TRUE, 0.75), digits = 1)

# Sputum eosinophils (%)
sum_G83[30, 1] <- round(nrow(G83_H[complete.cases(G83_H$Pct_Eosinophils), ] == TRUE), digits = 0)
sum_G83[31, 1] <- round(median(G83_H$Pct_Eosinophils, na.rm = TRUE), digits = 1)
sum_G83[32, 1] <- round(quantile(G83_H$Pct_Eosinophils, na.rm = TRUE, 0.25), digits = 1)
sum_G83[33, 1] <- round(quantile(G83_H$Pct_Eosinophils, na.rm = TRUE, 0.75), digits = 1)

sum_G83[30, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$Pct_Eosinophils), ] == TRUE), digits = 0)
sum_G83[31, 2] <- round(median(G83_MA$Pct_Eosinophils, na.rm = TRUE), digits = 1)
sum_G83[32, 2] <- round(quantile(G83_MA$Pct_Eosinophils, na.rm = TRUE, 0.25), digits = 1)
sum_G83[33, 2] <- round(quantile(G83_MA$Pct_Eosinophils, na.rm = TRUE, 0.75), digits = 1)

sum_G83[30, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$Pct_Eosinophils), ] == TRUE), digits = 0)
sum_G83[31, 3] <- round(median(G83_SA$Pct_Eosinophils, na.rm = TRUE), digits = 1)
sum_G83[32, 3] <- round(quantile(G83_SA$Pct_Eosinophils, na.rm = TRUE, 0.25), digits = 1)
sum_G83[33, 3] <- round(quantile(G83_SA$Pct_Eosinophils, na.rm = TRUE, 0.75), digits = 1)

# Sputum neutrophils (%)
sum_G83[34, 1] <- round(nrow(G83_H[complete.cases(G83_H$Pct_Neutrophils), ] == TRUE), digits = 0)
sum_G83[35, 1] <- round(median(G83_H$Pct_Neutrophils, na.rm = TRUE), digits = 1)
sum_G83[36, 1] <- round(quantile(G83_H$Pct_Neutrophils, na.rm = TRUE, 0.25), digits = 1)
sum_G83[37, 1] <- round(quantile(G83_H$Pct_Neutrophils, na.rm = TRUE, 0.75), digits = 1)

sum_G83[34, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$Pct_Neutrophils), ] == TRUE), digits = 0)
sum_G83[35, 2] <- round(median(G83_MA$Pct_Neutrophils, na.rm = TRUE), digits = 1)
sum_G83[36, 2] <- round(quantile(G83_MA$Pct_Neutrophils, na.rm = TRUE, 0.25), digits = 1)
sum_G83[37, 2] <- round(quantile(G83_MA$Pct_Neutrophils, na.rm = TRUE, 0.75), digits = 1)

sum_G83[34, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$Pct_Neutrophils), ] == TRUE), digits = 0)
sum_G83[35, 3] <- round(median(G83_SA$Pct_Neutrophils, na.rm = TRUE), digits = 1)
sum_G83[36, 3] <- round(quantile(G83_SA$Pct_Neutrophils, na.rm = TRUE, 0.25), digits = 1)
sum_G83[37, 3] <- round(quantile(G83_SA$Pct_Neutrophils, na.rm = TRUE, 0.75), digits = 1)

# Sputum macrophages (%)
sum_G83[38, 1] <- round(nrow(G83_H[complete.cases(G83_H$Pct_Macrophages), ] == TRUE), digits = 0)
sum_G83[39, 1] <- round(median(G83_H$Pct_Macrophages, na.rm = TRUE), digits = 1)
sum_G83[40, 1] <- round(quantile(G83_H$Pct_Macrophages, na.rm = TRUE, 0.25), digits = 1)
sum_G83[41, 1] <- round(quantile(G83_H$Pct_Macrophages, na.rm = TRUE, 0.75), digits = 1)

sum_G83[38, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$Pct_Macrophages), ] == TRUE), digits = 0)
sum_G83[39, 2] <- round(median(G83_MA$Pct_Macrophages, na.rm = TRUE), digits = 1)
sum_G83[40, 2] <- round(quantile(G83_MA$Pct_Macrophages, na.rm = TRUE, 0.25), digits = 1)
sum_G83[41, 2] <- round(quantile(G83_MA$Pct_Macrophages, na.rm = TRUE, 0.75), digits = 1)

sum_G83[38, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$Pct_Macrophages), ] == TRUE), digits = 0)
sum_G83[39, 3] <- round(median(G83_SA$Pct_Macrophages, na.rm = TRUE), digits = 1)
sum_G83[40, 3] <- round(quantile(G83_SA$Pct_Macrophages, na.rm = TRUE, 0.25), digits = 1)
sum_G83[41, 3] <- round(quantile(G83_SA$Pct_Macrophages, na.rm = TRUE, 0.75), digits = 1)

# Sputum lymphocytes (%)
sum_G83[42, 1] <- round(nrow(G83_H[complete.cases(G83_H$Pct_Lymphocytes), ] == TRUE), digits = 0)
sum_G83[43, 1] <- round(median(G83_H$Pct_Lymphocytes, na.rm = TRUE), digits = 1)
sum_G83[44, 1] <- round(quantile(G83_H$Pct_Lymphocytes, na.rm = TRUE, 0.25), digits = 1)
sum_G83[45, 1] <- round(quantile(G83_H$Pct_Lymphocytes, na.rm = TRUE, 0.75), digits = 1)

sum_G83[42, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$Pct_Lymphocytes), ] == TRUE), digits = 0)
sum_G83[43, 2] <- round(median(G83_MA$Pct_Lymphocytes, na.rm = TRUE), digits = 1)
sum_G83[44, 2] <- round(quantile(G83_MA$Pct_Lymphocytes, na.rm = TRUE, 0.25), digits = 1)
sum_G83[45, 2] <- round(quantile(G83_MA$Pct_Lymphocytes, na.rm = TRUE, 0.75), digits = 1)

sum_G83[42, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$Pct_Lymphocytes), ] == TRUE), digits = 0)
sum_G83[43, 3] <- round(median(G83_SA$Pct_Lymphocytes, na.rm = TRUE), digits = 1)
sum_G83[44, 3] <- round(quantile(G83_SA$Pct_Lymphocytes, na.rm = TRUE, 0.25), digits = 1)
sum_G83[45, 3] <- round(quantile(G83_SA$Pct_Lymphocytes, na.rm = TRUE, 0.75), digits = 1)

# Sputum mast cells (%)
sum_G83[46, 1] <- round(nrow(G83_H[complete.cases(G83_H$Pct_Mast_cells), ] == TRUE), digits = 0)
sum_G83[47, 1] <- round(median(G83_H$Pct_Mast_cells, na.rm = TRUE), digits = 1)
sum_G83[48, 1] <- round(quantile(G83_H$Pct_Mast_cells, na.rm = TRUE, 0.25), digits = 1)
sum_G83[49, 1] <- round(quantile(G83_H$Pct_Mast_cells, na.rm = TRUE, 0.75), digits = 1)

sum_G83[46, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$Pct_Mast_cells), ] == TRUE), digits = 0)
sum_G83[47, 2] <- round(median(G83_MA$Pct_Mast_cells, na.rm = TRUE), digits = 1)
sum_G83[48, 2] <- round(quantile(G83_MA$Pct_Mast_cells, na.rm = TRUE, 0.25), digits = 1)
sum_G83[49, 2] <- round(quantile(G83_MA$Pct_Mast_cells, na.rm = TRUE, 0.75), digits = 1)

sum_G83[46, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$Pct_Mast_cells), ] == TRUE), digits = 0)
sum_G83[47, 3] <- round(median(G83_SA$Pct_Mast_cells, na.rm = TRUE), digits = 1)
sum_G83[48, 3] <- round(quantile(G83_SA$Pct_Mast_cells, na.rm = TRUE, 0.25), digits = 1)
sum_G83[49, 3] <- round(quantile(G83_SA$Pct_Mast_cells, na.rm = TRUE, 0.75), digits = 1)

# FeNO (ppb)
sum_G83[50, 1] <- round(nrow(G83_H[complete.cases(G83_H$FeNO), ] == TRUE), digits = 0)
sum_G83[51, 1] <- round(median(G83_H$FeNO, na.rm = TRUE), digits = 0)
sum_G83[52, 1] <- round(quantile(G83_H$FeNO, na.rm = TRUE, 0.25), digits = 0)
sum_G83[53, 1] <- round(quantile(G83_H$FeNO, na.rm = TRUE, 0.75), digits = 0)

sum_G83[50, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$FeNO), ] == TRUE), digits = 0)
sum_G83[51, 2] <- round(median(G83_MA$FeNO, na.rm = TRUE), digits = 0)
sum_G83[52, 2] <- round(quantile(G83_MA$FeNO, na.rm = TRUE, 0.25), digits = 0)
sum_G83[53, 2] <- round(quantile(G83_MA$FeNO, na.rm = TRUE, 0.75), digits = 0)

sum_G83[50, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$FeNO), ] == TRUE), digits = 0)
sum_G83[51, 3] <- round(median(G83_SA$FeNO, na.rm = TRUE), digits = 0)
sum_G83[52, 3] <- round(quantile(G83_SA$FeNO, na.rm = TRUE, 0.25), digits = 0)
sum_G83[53, 3] <- round(quantile(G83_SA$FeNO, na.rm = TRUE, 0.75), digits = 0)

# FEV1 (%)
sum_G83[54, 1] <- round(nrow(G83_H[complete.cases(G83_H$FEV1_Pct_.L.), ] == TRUE), digits = 0)
sum_G83[55, 1] <- round(median(G83_H$FEV1_Pct_.L., na.rm = TRUE), digits = 1)
sum_G83[56, 1] <- round(quantile(G83_H$FEV1_Pct_.L., na.rm = TRUE, 0.25), digits = 1)
sum_G83[57, 1] <- round(quantile(G83_H$FEV1_Pct_.L., na.rm = TRUE, 0.75), digits = 1)

sum_G83[54, 2] <- round(nrow(G83_MA[complete.cases(G83_MA$FEV1_Pct_.L.), ] == TRUE), digits = 0)
sum_G83[55, 2] <- round(median(G83_MA$FEV1_Pct_.L., na.rm = TRUE), digits = 1)
sum_G83[56, 2] <- round(quantile(G83_MA$FEV1_Pct_.L., na.rm = TRUE, 0.25), digits = 1)
sum_G83[57, 2] <- round(quantile(G83_MA$FEV1_Pct_.L., na.rm = TRUE, 0.75), digits = 1)

sum_G83[54, 3] <- round(nrow(G83_SA[complete.cases(G83_SA$FEV1_Pct_.L.), ] == TRUE), digits = 0)
sum_G83[55, 3] <- round(median(G83_SA$FEV1_Pct_.L., na.rm = TRUE), digits = 1)
sum_G83[56, 3] <- round(quantile(G83_SA$FEV1_Pct_.L., na.rm = TRUE, 0.25), digits = 1)
sum_G83[57, 3] <- round(quantile(G83_SA$FEV1_Pct_.L., na.rm = TRUE, 0.75), digits = 1)

# Eczema
sum_G83[58, 1] <- nrow(subset(metadata_G83, Eczema_Diagnosed == "yes" & blood == "Healthy, non-smoking"))
sum_G83[59, 1] <- round((sum_G83[58, 1]/sum_G83[1, 1])*100, digits = 1)

sum_G83[58, 2] <- nrow(subset(metadata_G83, Eczema_Diagnosed == "yes" & blood == "Moderate asthma, non-smoking"))
sum_G83[59, 2] <- round((sum_G83[58, 2]/sum_G83[1, 2])*100, digits = 1)

sum_G83[58, 3] <- nrow(subset(metadata_G83, Eczema_Diagnosed == "yes" & (blood == "Severe asthma, non-smoking" | blood == "Severe asthma, smoking")))
sum_G83[59, 3] <- round((sum_G83[58, 3]/sum_G83[1, 3])*100, digits = 1)

# Allergic Rhinitis
sum_G83[60, 1] <- nrow(subset(metadata_G83, Allergic_Rhinitis == "yes" & blood == "Healthy, non-smoking"))
sum_G83[61, 1] <- round((sum_G83[60, 1]/sum_G83[1, 1])*100, digits = 1)

sum_G83[60, 2] <- nrow(subset(metadata_G83, Allergic_Rhinitis == "yes" & blood == "Moderate asthma, non-smoking"))
sum_G83[61, 2] <- round((sum_G83[60, 2]/sum_G83[1, 2])*100, digits = 1)

sum_G83[60, 3] <- nrow(subset(metadata_G83, Allergic_Rhinitis == "yes" & (blood == "Severe asthma, non-smoking" | blood == "Severe asthma, smoking")))
sum_G83[61, 3] <- round((sum_G83[60, 3]/sum_G83[1, 3])*100, digits = 1)

# Atopy
sum_G83[62, 1] <- nrow(subset(metadata_G83, Atopy == "positive" & blood == "Healthy, non-smoking"))
sum_G83[63, 1] <- round((sum_G83[62, 1]/sum_G83[1, 1])*100, digits = 1)

sum_G83[62, 2] <- nrow(subset(metadata_G83, Atopy == "positive" & blood == "Moderate asthma, non-smoking"))
sum_G83[63, 2] <- round((sum_G83[62, 2]/sum_G83[1, 2])*100, digits = 1)

sum_G83[62, 3] <- nrow(subset(metadata_G83, Atopy == "positive" & (blood == "Severe asthma, non-smoking" | blood == "Severe asthma, smoking")))
sum_G83[63, 3] <- round((sum_G83[62, 3]/sum_G83[1, 3])*100, digits = 1)

# Sinusitis
sum_G83[64, 1] <- nrow(subset(metadata_G83, Sinusitis_Diagnosed == "yes" & blood == "Healthy, non-smoking"))
sum_G83[65, 1] <- round((sum_G83[64, 1]/sum_G83[1, 1])*100, digits = 1)

sum_G83[64, 2] <- nrow(subset(metadata_G83, Sinusitis_Diagnosed == "yes" & blood == "Moderate asthma, non-smoking"))
sum_G83[65, 2] <- round((sum_G83[64, 2]/sum_G83[1, 2])*100, digits = 1)

sum_G83[64, 3] <- nrow(subset(metadata_G83, Sinusitis_Diagnosed == "yes" & (blood == "Severe asthma, non-smoking" | blood == "Severe asthma, smoking")))
sum_G83[65, 3] <- round((sum_G83[64, 3]/sum_G83[1, 3])*100, digits = 1)

# Nasal Polyps
sum_G83[66, 1] <- nrow(subset(metadata_G83, Nasal_Polyps_Diagnosed == "yes" & blood == "Healthy, non-smoking"))
sum_G83[67, 1] <- round((sum_G83[66, 1]/sum_G83[1, 1])*100, digits = 1)

sum_G83[66, 2] <- nrow(subset(metadata_G83, Nasal_Polyps_Diagnosed == "yes" & blood == "Moderate asthma, non-smoking"))
sum_G83[67, 2] <- round((sum_G83[66, 2]/sum_G83[1, 2])*100, digits = 1)

sum_G83[66, 3] <- nrow(subset(metadata_G83, Nasal_Polyps_Diagnosed == "yes" & (blood == "Severe asthma, non-smoking" | blood == "Severe asthma, smoking")))
sum_G83[67, 3] <- round((sum_G83[66, 3]/sum_G83[1, 3])*100, digits = 1)

# Oral Corticosteroid Use
sum_G83[68, 1] <- nrow(subset(metadata_G83, oral_corticosteroids == "yes" & blood == "Healthy, non-smoking"))
sum_G83[69, 1] <- round((sum_G83[68, 1]/sum_G83[1, 1])*100, digits = 1)

sum_G83[68, 2] <- nrow(subset(metadata_G83, oral_corticosteroids == "yes" & blood == "Moderate asthma, non-smoking"))
sum_G83[69, 2] <- round((sum_G83[68, 2]/sum_G83[1, 2])*100, digits = 1)

sum_G83[68, 3] <- nrow(subset(metadata_G83, oral_corticosteroids == "yes" & (blood == "Severe asthma, non-smoking" | blood == "Severe asthma, smoking")))
sum_G83[69, 3] <- round((sum_G83[68, 3]/sum_G83[1, 3])*100, digits = 1)

# Race: white caucasian
sum_G83[70, 1] <- nrow(subset(metadata_G83, Race == "race: white_caucasian" & blood == "Healthy, non-smoking"))
sum_G83[71, 1] <- round((sum_G83[70, 1]/sum_G83[1, 1])*100, digits = 1)

sum_G83[70, 2] <- nrow(subset(metadata_G83, Race == "race: white_caucasian" & blood == "Moderate asthma, non-smoking"))
sum_G83[71, 2] <- round((sum_G83[70, 2]/sum_G83[1, 2])*100, digits = 1)

sum_G83[70, 3] <- nrow(subset(metadata_G83, Race == "race: white_caucasian" & (blood == "Severe asthma, non-smoking" | blood == "Severe asthma, smoking")))
sum_G83[71, 3] <- round((sum_G83[70, 3]/sum_G83[1, 3])*100, digits = 1)

