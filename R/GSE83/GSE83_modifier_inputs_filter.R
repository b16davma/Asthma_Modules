################################################################
#INPUT 1: data frame of HGNC symbols/samples of p-values (1vsH)#
################################################################

rm(list = ls())
library(limma)
library(GEOquery)
library(oligo)
library(foreach)
library(parallel)
library(doParallel)
#library(data.table)

## Obtain Affy IDs/samples matrix of p-values
# Download expression set
gset <- getGEO("GSE69683")
ex <- exprs(gset)

# Obtain expression matrix of H/SA samples
acc_samples_blood <- endotype_list[, c(1, 34)][complete.cases(endotype_list[, 1]), ]
acc_samples_blood_H <- acc_samples_blood
acc_samples_blood_H$Category <- c(rep("NA", length(acc_samples_blood_H$Accession)))
acc_samples_blood_H$Category[grep("Healthy|healthy", acc_samples_blood_H$blood)] <- "Healthy"
acc_samples_blood_H <- acc_samples_blood_H[acc_samples_blood_H$Category == "Healthy", ]
ex_H <- ex[, colnames(ex) %in% acc_samples_blood_H$Accession]

acc_samples_blood_SA <- acc_samples_blood
acc_samples_blood_SA$Category <- c(rep("NA", length(acc_samples_blood_SA$Accession)))
acc_samples_blood_SA$Category[grep("Severe|severe", acc_samples_blood_SA$blood)] <- "Severe asthma"
acc_samples_blood_SA <- acc_samples_blood_SA[acc_samples_blood_SA$Category == "Severe asthma", ]
ex_SA <- ex[, colnames(ex) %in% acc_samples_blood_SA$Accession]

matrix_A <- matrix(data = 0, nrow = ncol(ex_H) + 1, ncol = 2)
matrix_A[, 1] <- c(0, rep(1, 87))
matrix_A[, 2] <- c(1, rep(0, 87))
colnames(matrix_A) <- c("A", "B")
matrix_B <- matrix(data = NA, nrow = nrow(ex_SA), ncol = ncol(ex_SA))
rownames(matrix_B) <- rownames(ex_SA)
colnames(matrix_B) <- colnames(ex_SA)
ex_design <- matrix(data = 0, nrow = nrow(ex_H), ncol = ncol(ex_H) + 1)
rownames(ex_design) <- rownames(gset)
colnames(ex_design) <- c("GSM_SA", colnames(ex_H))
ex_design[, 2:88] <- ex_H

cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(ex_SA), .packages = "limma") %do% {
  ex_design[, 1] <- ex_SA[, i]
  design <- matrix_A
  fit <- lmFit(ex_design, design)
  contrast.matrix <- makeContrasts(test = B-A, levels = design)
  fit2 <- contrasts.fit(fit, contrast.matrix)
  fit2 <- eBayes(fit2)
  probe_1_sig <- topTable(fit2, n = Inf, coef = 1, adjust = "fdr")
  probe_1_sig <- probe_1_sig[match(rownames(matrix_B), rownames(probe_1_sig)), ]
  matrix_B[, i] <- probe_1_sig$P.Value
  print(paste("Sample", i, "is added to matrix_B", sep = " "))
}
stopCluster(cl)

# Filter probes by logFC (cutoff established by tT_filt: >= 0.3 & <= -0.3)
matrix_Bfilt <- matrix_B[rownames(matrix_B) %in% tT_filt$ID, ]

# Extract GPL (array platform) info
source("https://bioconductor.org/biocLite.R")
biocLite("GEOquery")
library(GEOquery)
gpl <- getGEO("GPL13158", destdir = ".")
Meta(gpl)$title
annot_info <- Table(gpl)[, c(1, 11, 12)] # 16 levels/annotation formats

# Add HGNC symbols to Affy IDs/samples matrix of p-values
matrix_C <- matrix(data = NA, nrow = nrow(matrix_Bfilt), ncol = (ncol(matrix_Bfilt) + 1))
annot_infofilt <- annot_info[annot_info$ID %in% tT_filt$ID, ]
rownames(matrix_C) <- annot_infofilt[, 2]
matrix_C[, 1] <- rownames(matrix_Bfilt)
colnames(matrix_C)[2:335] <- colnames(matrix_Bfilt)
colnames(matrix_C)[1] <- "Affy_ID"
matrix_C <- as.data.frame(matrix_C)
matrix_C[, 1] <- as.character(matrix_C[, 1])
matrix_C[, 2:335] <- as.numeric(as.matrix(round(matrix_Bfilt[, 1:334], digits = 8)))


# Remove incomplete cases for HGNC symbols
matrix_D <- matrix_C
rownames(matrix_D) <- matrix_C[, 1]
matrix_D[, 1] <- rownames(matrix_C)
colnames(matrix_D)[1] <- "HGNC_Symbol"
is.na(matrix_D[, 1]) <- which(matrix_D[, 1] == "") 
matrix_D <- matrix_D[complete.cases(matrix_D[, 1]), ]

# Combine probes related to the same gene (keeping the lowest p-value for each sample), conserving the order
library(plyr)
keeping.order <- function(data, fn, ...) { 
  col <- ".sortColumn"
  data[, col] <- 1:nrow(data)
  out <- fn(data, ...) 
  if (!col %in% colnames(out)) stop("Ordering column not preserved by function") 
  out <- out[order(out[, col]), ] 
  out[, col] <- NULL 
  out 
}

matrix_E <- keeping.order(matrix_D, ddply, .(matrix_D$HGNC_Symbol), numcolwise(min))
rownames(matrix_E) <- matrix_E[, 1]
matrix_E[1] <- NULL

# Duplication and expansion of input rows with 2+ HGNC Gene Symbols (separated by ///)
matrix_H <- matrix_E
matrix_H$HGNC_Symbol <- rownames(matrix_H)
interesting_fields <- c("HGNC_Symbol", colnames(matrix_E))
matrix_H <- matrix_H[interesting_fields]
to_be_duplicated <- c(colnames(matrix_E))
to_be_expanded <- c("HGNC_Symbol")

# Splitting conditions
matrix_split1 <- strsplit(matrix_H$HGNC_Symbol, split = " /// ")
#matrix_split2 <- strsplit(matrix_H$X, split = " /// ")

# Duplication of rows
matrix_temp <- matrix_H[, to_be_duplicated]
matrix_I <- matrix_temp[rep(seq_len(nrow(matrix_temp)), sapply(matrix_split1, length)), ]

# Expansion of rows (same p-value)
matrix_I[, to_be_expanded[1]] <- unlist(matrix_split1)
#matrix_I[, to_be_expanded[2]] <- unlist(matrix_split2)

# Removal of duplicated rows (lowest p-value row is kept)
matrix_J <- keeping.order(matrix_I, ddply, .(matrix_I$HGNC_Symbol), numcolwise(min))
rownames(matrix_J) <- matrix_J[, 1]
matrix_J[1] <- NULL

# Output
#matrix_J <- setDT(matrix_J, keep.rownames = TRUE)[]
#colnames(matrix_J)[1] <- "HGNC_Symbol"
#matrix_J <- as.data.frame(matrix_J)
matrix_J[, 335] <- rownames(matrix_J)
colnames(matrix_J)[335] <- "HGNC_Symbol"
write.table(matrix_J, file = "matrix_J.txt", row.names = TRUE, col.names = TRUE, sep = ",", dec = ".")


################################################################
#INPUT 2: data frames of Entrez IDs/samples, ranked by p-value #
################################################################

matrix_F <- matrix_E
annot_unique <- annot_info[annot_info[,2] %in% rownames(matrix_F), ]
annot_unique <- annot_unique[!duplicated(annot_unique[, 2]), ]
matrix_F <- as.matrix(matrix_F)
rownames(matrix_F) <- annot_unique[, 3]
matrix_F <- as.data.frame(matrix_F)

matrix_G <- matrix(data = NA, nrow = nrow(matrix_F), ncol = 2)
colnames(matrix_G) <- c("Entrez_ID", "P.Val")
matrix_G <- as.data.frame(matrix_G)
matrix_G[, 1] <- as.character(matrix_G[, 1])
matrix_G[, 2] <- as.numeric(matrix_G[, 2])

# Obtain Entrez IDs/samples matrix of p-values
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(matrix_F)) %dopar% {
  matrix_ <- matrix_G
  matrix_[, 1] <- rownames(matrix_F)
  matrix_[, 2] <- matrix_F[, i]
  matrix_ <- matrix_[order(matrix_$P.Val, decreasing = FALSE), ]
  assign(paste("matrix_", colnames(matrix_F)[i], sep = ""), matrix_)
  print(paste("matrix_", colnames(matrix_F)[i], " has been created", sep = ""))
}
stopCluster(cl)


################################################################
#INPUT 3: data frame of Entrez IDs/samples of p-values         #
################################################################

# Duplication and expansion of input rows with 2+ Entrez IDs (separated by ///)
matrix_K <- matrix_F
matrix_K$Entrez_ID <- rownames(matrix_K)
interesting_fields <- c("Entrez_ID", colnames(matrix_F))
matrix_K <- matrix_K[interesting_fields]
to_be_duplicated <- c(colnames(matrix_F))
to_be_expanded <- c("Entrez_ID")

# Splitting conditions
matrix_split1 <- strsplit(matrix_K$Entrez_ID, split = " /// ")
#matrix_split2 <- strsplit(matrix_K$X, split = " /// ")

# Duplication of rows
matrix_temp <- matrix_K[, to_be_duplicated]
matrix_K <- matrix_temp[rep(seq_len(nrow(matrix_temp)), sapply(matrix_split1, length)), ]

# Expansion of rows (same p-value)
matrix_K[, to_be_expanded[1]] <- unlist(matrix_split1)
#matrix_K[, to_be_expanded[2]] <- unlist(matrix_split2)

# Removal of duplicated rows (lowest p-value row is kept)
matrix_K <- keeping.order(matrix_K, ddply, .(matrix_K$Entrez_ID), numcolwise(min))
rownames(matrix_K) <- matrix_K[, 1]
matrix_K[1] <- NULL

# Output
#matrix_K <- setDT(matrix_K, keep.rownames = TRUE)[]
#colnames(matrix_K)[1] <- "Entrez_ID"
#matrix_K <- as.data.frame(matrix_K)
matrix_K[, 335] <- rownames(matrix_K)
colnames(matrix_K)[335] <- "Entrez_ID"
write.table(matrix_K, file = "matrix_K.txt", row.names = TRUE, col.names = TRUE, sep = ",", dec = ".")


################################################################
# Result interpretation                                        #
################################################################
# matrix_A: 88x2 matrix equivalent to the design matrix, prior to matrix_Bfilt
# matrix_B: 54715x334 matrix of Affy IDs/Samples, with p-values
# matrix_Bfilt: 1370x334 matrix of Affy IDs/Samples, with p-values (filtered by tT_filt)
# matrix_C: 1370x335 data frame of HGNC Symbols/Samples, with p-values (+ Affy IDs)
# matrix_D: 896x335 data frame of Affy IDs/Samples, with p-values (+ HGNC Symbols, only complete cases)
# matrix_E: 743x334 data frame of HGNC Symbols/Samples, with p-values (repeated HGNC Symbols combined for the minimal p-value)
# matrix_F: 743x334 data frame of Entrez IDs/Samples, with p-values
# matrix_G: 743x2 data frame, template for addition of Entrez IDs and p-values
# matrix_H: 743x335 data frame of HGNC Symbols/Samples, with p-values (+ HGNC Symbols), template for duplication and expansion
# matrix_I: 812x335 data frame of HGNC Symbols/Samples, with p-values (+ HGNC Symbols), output of row duplication and expansion
# matrix_J: 786x335 data frame of HGNC Symbols/Samples, with p-values (+ HGNC Symbols), output of unique function [INPUT 1A]
# matrix_GSMXXXXXXX: 786x2 data frames of Entrez IDs and p-values for a single sample (ranked by increasing p-value) [INPUT 2]
# matrix_K: 786x335 data frame of Entrez IDs/Samples, with p-values (+ Entrez IDs), output of unique function [INPUT 1B]
