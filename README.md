**1.- Project title**

Identification of personalized multi-omic disease modules of asthma

**2.- Aim**

The objective of the project is the investigation and identification of multi-omic endotypes in asthma by the combination of individual genetical and expression modules.

**3.- Abstract**

Difficulties in the diagnosis and therapeutic treatment of many common diseases often stem from the high level of heterogeneity displayed by their underlying disease mechanisms, generally involving from dozens to hundreds of interconnected disease-associated genes with distinct effects. While the task of individually identifying these genes and their products can be facilitated by high-throughput techniques, such as omics or genome-wide association studies (GWAS), comprehending and dissecting their functionality as a whole remains an arduous challenge. The yet emerging field of systems medicine applies network-based analyses, in combination with clinical data, to predict and classify these genes by using different approaches, including linkage methods, diffusion-based methods, and disease module-based methods. The latter are founded on the assumption that all cellular components that belong to network neighbourhoods with an above-average local density ("topological modules"), which present biologically related functions ("functional modules"), or which jointly contribute to a cellular function whose perturbation leads to the development of a certain disease phenotype ("disease modules") have a high chance of being linked to a specific disease. In other words, it hypothesizes that topological, functional and disease modules overlap to some extent, so that a disease arises when a functional module, formed by topologically close genes or products, is disrupted.

The analysis of disease modules applies conventional network principles to provide novel insights into disease mechanisms, biomarkers and therapeutic targets. In turn, disease modules obtained by various high-throughput techniques can be structured as network layers according to their components (DNA, mRNA, proteins, metabolites, lipids, single nucleotide polymorphisms, etc.) and then superimposed in order to form multilayer disease modules (MLDMs), based on the links between the variables on each layer. MLDMs serve as valuable tools in the identification of multi-layered diagnostic markers, can help in the design of human disease networks by linking modules formed by clinical data, and display a remarkable potential in disease progression tracking. While several limitations to the use of MLDMs do exist, such as the difficulty to obtain a sample size sufficiently large to produce statistically significant results or the simultaneous involvement of multiple cell types in a disease, MLDMs are regarded as promising templates for the integration and study of disease relevant data.

Asthma is a complex and heterogeneous condition associated with the chronic inflammation and remodelling of the airways. It involves very diverse observable characteristics (phenotypes) and biological mechanisms (endotypes), thus being generally considered as a collection of symptoms rather than a single disease. Almost 300 million people worldwide are affected by asthma, of which between 5-10 % are diagnosed with severe forms of the disease that currently lack an adequate treatment and account for a substantial healthcare cost. By precisely identifying the MLDMs behind asthma endotypes, the disease subtypes may be further characterized and stratified, on the way to a more effective treatment. For this purpose, the computational analysis of over 20,000 individual samples from an asthma study, as well as the multi-cellular gene expression and genetics from about 500 individuals, will be performed by the use of a diverse range of software environments, such as R, Python and Bash.

**4.- Methods**

*Gene expression microarray, RNA-Seq and Whole Genome Sequencing (WGS) analysis:*  computational analysis of the data procured by previous genomic and transcriptomic studies in asthma patients, using R, Python and Bash, among other software.

*Genome-Wide Association Studies (GWAS):*  the information obtained in GWAS will help to develop the individualized modules for each patient, by overlapping them statistically and biologically across their endotype specific modules to identify the mechanism and to generate new hypothesis for treatment and drug testing.  

*Network analysis:*  creation and refining of robust modules, primarily using R packages, from the data obtained in the gene expression microarray, RNA-Seq and WGS analysis.

*Clustering (supervised):*  part of validation and cross check at every point of the results, with the objective of determining whether the obtained dysregulated genes were able to classify the endotypes of asthma or viceversa – could endotypes be classified using the obtained modules.  

*Annotation enrichment analysis:*  annotation criteria and identifier mapping (Gene symbol, Entrez-Ids, Ensembl-Ids, protein Ids, etc.) that will be carried out along the rest of the steps, guaranteeing their adequate analysis and result interpretation.

